package ru.tsc.almukhametov.tm.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasFinishDate {

    @Nullable
    Date getFinishDate();

    void getFinishDate(@Nullable Date startDate);

}
