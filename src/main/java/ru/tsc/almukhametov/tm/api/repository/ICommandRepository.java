package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArg();

    @NotNull
    AbstractCommand getCommandByName(String name);

    @NotNull
    AbstractCommand getCommandByArg(String arg);

    void add(@NotNull AbstractCommand command);

}
