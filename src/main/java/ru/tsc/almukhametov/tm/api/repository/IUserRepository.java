package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    Optional<User> findByLogin(@NotNull String login);

    @Nullable
    Optional<User> findByEmail(@NotNull String email);

    @Nullable
    Optional<User> removeUser(@NotNull User user);

    @Nullable
    Optional<User> removeUserById(@NotNull String id);

    @Nullable
    Optional<User> removeUserByLogin(@NotNull String login);

    @Nullable
    User updateUser(@NotNull String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    User setPassword(@NotNull String userId, @NotNull String password);

}