package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    Optional<Task> findByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Optional<Task> removeByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Task startById(@NotNull String userId, @Nullable String id);

    @NotNull
    Task startByIndex(@NotNull String userId, Integer id);

    @NotNull
    Task startByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Task finishById(@NotNull String userId, @Nullable String id);

    @NotNull
    Task finishByIndex(@NotNull String userId, Integer index);

    @NotNull
    Task finishByName(@NotNull String userId, @Nullable String name);

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, Integer index, @NotNull Status status);

    @NotNull
    Task changeTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @NotNull
    List<Task> removeAllTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    @NotNull
    Task bindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

}
