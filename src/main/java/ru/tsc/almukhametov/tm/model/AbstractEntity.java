package ru.tsc.almukhametov.tm.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

public abstract class AbstractEntity {

    @Getter
    @Setter
    protected String id = UUID.randomUUID().toString();

}
