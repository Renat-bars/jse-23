package ru.tsc.almukhametov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.comparator.*;

import java.util.Comparator;

public enum Sort {

    @NotNull
    NAME("Sort by name", ComparatorByName.getInstance()),
    @NotNull
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    @NotNull
    START_DATE("Sort by date start", ComparatorByStartDate.getInstance()),
    @NotNull
    DATE_FINISH("Sort by finish date", ComparatorByFinishDate.getInstance()),
    @NotNull
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator comparator;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
