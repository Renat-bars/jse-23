package ru.tsc.almukhametov.tm.exception.system;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class ProcessException extends AbstractException {

    public ProcessException() {
        super("Process error, try again");
    }
}
