package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IUserRepository;
import ru.tsc.almukhametov.tm.model.User;
import ru.tsc.almukhametov.tm.util.HashUtil;

import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    protected Predicate<User> predicateByLogin(final String login) {
        return u -> login.equals(u.getLogin());
    }

    protected Predicate<User> predicateByEmail(final String email) {
        return u -> email.equals(u.getEmail());
    }

    @Nullable
    @Override
    public Optional<User> findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @Nullable
    @Override
    public Optional<User> findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @Nullable
    @Override
    public Optional<User> removeUser(@Nullable final User user) {
        list.remove(user);
        return Optional.ofNullable(user);
    }

    @Nullable
    @Override
    public Optional<User> removeUserById(@NotNull final String id) {
        @Nullable final Optional<User> user = findById(id);
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public Optional<User> removeUserByLogin(@NotNull final String login) {
        @Nullable final Optional<User> user = findByLogin(login);
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User updateUser(@NotNull final String userId, final @Nullable String firstName, final @Nullable String lastName, final @Nullable String middleName) {
        @Nullable final Optional<User> user = findById(userId);
        user.ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setMiddleName(middleName);
        });
        return user.orElse(null);
    }

    @Nullable
    @Override
    public User setPassword(@NotNull final String userId, @NotNull final String password) {
        @Nullable final Optional<User> user = findById(userId);
        user.ifPresent(u -> u.setPasswordHash(HashUtil.salt(password)));
        return user.orElse(null);
    }

}