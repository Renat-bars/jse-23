package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Optional<Task> findByName(@NotNull final String userId, @Nullable final String name) {
        return Optional.ofNullable(findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new));
    }

    @NotNull
    @Override
    public Optional<Task> removeByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Task> task = (findByName(userId, name));
        task.ifPresent(this::remove);
        return Optional.ofNullable(task.orElseThrow(TaskNotFoundException::new));
    }

    @NotNull
    @Override
    public Task startById(@NotNull final String userId, @Nullable final String id) {
        @Nullable final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    public Task startByIndex(@NotNull final String userId, final Integer index) {
        @Nullable final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    public Task startByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.IN_PROGRESS));
        return task.get();
    }

    @NotNull
    @Override
    public Task finishById(@NotNull final String userId, @Nullable final String id) {
        @Nullable final Optional<Task> task = findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    public Task finishByIndex(@NotNull final String userId, final Integer index) {
        @Nullable final Optional<Task> task = findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    public Task finishByName(@NotNull final String userId, @Nullable final String name) {
        @Nullable final Optional<Task> task = findByName(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(t -> t.setStatus(Status.COMPLETED));
        return task.get();
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        @Nullable final Optional<Task> task = findById(userId, id);
        task.ifPresent(t -> t.setStatus(status));
        return task.get();
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(@NotNull final String userId, final Integer index, @NotNull final Status status) {
        @Nullable final Optional<Task> task = findByIndex(userId, index);
        task.ifPresent(t -> t.setStatus(status));
        return task.get();
    }

    @NotNull
    @Override
    public Task changeTaskStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        @Nullable final Optional<Task> task = findByName(userId, name);
        task.ifPresent(t -> t.setStatus(status));
        return task.get();
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @Nullable final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                tasksByProject.add(task);
        }
        return tasksByProject;
    }

    @NotNull
    @Override
    public List<Task> removeAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        for (final Task task : list) {
            if (projectId.equals(task.getProjectId()))
                task.setProjectId(null);
        }
        return list;
    }

    @NotNull
    @Override
    public Task bindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(projectId));
        return task.get();
    }

    @NotNull
    @Override
    public Task unbindTaskById(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Optional<Task> task = findById(userId, taskId);
        task.ifPresent(t -> t.setProjectId(null));
        return task.get();
    }

}
