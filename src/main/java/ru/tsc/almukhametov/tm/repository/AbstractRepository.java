package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Nullable
    public E add(@Nullable final E entity) {
        list.add(entity);
        return entity;
    }

    @Nullable
    public Optional<E> remove(@Nullable final E entity) {
        list.remove(entity);
        return Optional.ofNullable(entity);
    }

    @NotNull
    public List<E> findAll() {
        return list;
    }

    @NotNull
    public List<E> findAll(@Nullable final Comparator<E> comparator) {
        final List<E> entities = new ArrayList<>(this.list);
        entities.sort(comparator);
        return entities;
    }

    public void clear() {
        list.removeAll(list);
    }

    @Nullable
    public Optional<E> findById(@Nullable final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst();
    }

    @Nullable
    public Optional<E> findByIndex(final Integer index) {
        return Optional.ofNullable(list.get(index));
    }

    @Nullable
    public Optional<E> removeById(@Nullable final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Nullable
    public Optional<E> removeByIndex(final Integer index) {
        final Optional<E> entity = findByIndex(index);
        entity.ifPresent(this::remove);
        return entity;

    }

    public boolean existById(@Nullable final String id) {
        final Optional<E> entity = findById(id);
        return entity.isPresent();
    }

    public boolean existByIndex(final int index) {
        final List<E> list = findAll();
        if (index < 0) return false;
        return index < list.size();
    }

    public Integer getSize() {
        return list.size();
    }

}
