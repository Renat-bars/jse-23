package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.IOwnerRepository;
import ru.tsc.almukhametov.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    protected Predicate<E> predicateByUserId(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    protected Predicate<E> predicateById(final String id) {
        return e -> id.equals(e.getId());
    }

    @NotNull
    public E add(@NotNull final String userId, @Nullable final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    public void remove(@NotNull final String userId, @Nullable final E entity) {
        if (!userId.equals(entity.getUserId())) return;
        list.remove(entity);
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final Comparator<E> comparator) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void clear(@NotNull final String userId) {
        final List<E> list = findAll(userId);
        this.list.removeAll(list);
    }

    @Nullable
    public Optional<E> findById(@NotNull final String userId, @Nullable final String id) {
        return list.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateById(id))
                .findFirst();
    }

    @Nullable
    public Optional<E> findByIndex(@NotNull final String userId, final Integer index) {
        return list.stream()
                .filter(e -> e.getUserId().equals(userId))
                .skip(index)
                .findFirst();
    }

    @Nullable
    public Optional<E> removeById(@NotNull final String userId, @Nullable final String id) {
        final Optional<E> entity = findById(userId, id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Nullable
    public Optional<E> removeByIndex(@NotNull final String userId, final Integer index) {
        final Optional<E> entity = findByIndex(userId, index);
        entity.ifPresent(this::remove);
        return entity;
    }

    public boolean existById(@NotNull final String userId, @Nullable final String id) {
        final Optional<E> entity = findById(userId, id);
        return entity.isPresent();
    }

    public boolean existByIndex(@NotNull final String userId, final int index) {
        final List<E> list = findAll(userId);
        if (index < 0) return false;
        return index < list.size();
    }

}
