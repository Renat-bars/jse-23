package ru.tsc.almukhametov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.api.repository.ICommandRepository;
import ru.tsc.almukhametov.tm.command.AbstractCommand;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name == null || name.isEmpty()) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg == null || arg.isEmpty()) continue;
            result.add(arg);
        }
        return result;
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        final String name = command.name();
        final String arg = command.arg();
        if (name != null) commands.put(name, command);
        if (arg != null) arguments.put(arg, command);
    }

}
